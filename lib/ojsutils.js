/*jshint node: true, -W032, -W106*/
'use strict';

module.exports = function (types) {

    if (false === Array.isArray(types)) {
        throw new Error('you should pass an array!!');
    };

    var _instanceOf = function (value) {

            var typesLength = types.length;
            for (var i = 0; i < typesLength; i++) {
                if (value instanceof types[i]) {
                    return true;
                };
            };
            return false;
        };


    //http://stackoverflow.com/questions/16564606/how-to-get-the-values-of-sub-objects-of-json-objects-while-using-object-key-meth
    var _flatten = function (data, result, path, keyOrValueOrBoth) {
            var key, value, p;
            result = result || [];
            path = path || '';

            var arrayOfKeys = Object.keys(data);
            for (var i = 0; i < arrayOfKeys.length; i++) {
                key = arrayOfKeys[i];
                value = data[key];
                p = path + key;

                if (!data.hasOwnProperty(key)) {
                    continue;
                }

                if (value && typeof value === 'object' && !Array.isArray(value) &&
                //!(value instanceof Date || value instanceof RegExp || value instanceof ObjectId )) {
                !(value instanceof Date || value instanceof RegExp || _instanceOf(value))) {

                    _flatten(value, result, p + '.', keyOrValueOrBoth);
                } else {
                    switch (keyOrValueOrBoth) {
                    case 1:
                        result.push([p, value]);
                        break;
                    case 2:
                        result.push(p);
                        break;
                    case 3:
                        result.push(value);
                        break;
                    default:
                        result.push([p, value]);
                    };
                }
            };

            return result;
        };

    var _flattenKeyValue = function (data) {
            return _flatten(data, [], '', 1);
        };

    var _flattenKey = function (data) {
            return _flatten(data, [], '', 2);
        };

    var _flattenValue = function (data) {
            return _flatten(data, [], '', 3);
        };


    var _getSubPropertyByString = function (o, s) {
            s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
            s = s.replace(/^\./, ''); // strip leading dot
            var a = s.split('.');
            while (a.length) {
                var n = a.shift();
                if (n in o) {
                    o = o[n];
                } else {
                    return;
                }
            }
            return o;
        };

    //http://stackoverflow.com/questions/13719593/javascript-how-to-set-object-property-given-its-string-name
    var _setSubPropertyByString = function (obj, prop, value) {
            if (typeof prop === 'string') {
                prop = prop.split('.');
            };

            var subprop = null;
            var e = null;
            if (prop.length > 1) {
                e = prop.shift();
                subprop = Object.prototype.toString.call(obj[e]) === '[object Object]' ? obj[e] : {};
                if (undefined === obj[e]) {
                    obj[e] = subprop;
                };
                _setSubPropertyByString(subprop, prop, value);
            } else {
                obj[prop[0]] = value;
            }
            //obj[prop[0]] = value;
        };

    var _unflattenFromCouples = function (keys, values) {
            var obj = {};
            var keysLenght = keys.length;

            for (var i = 0; i < keysLenght; i++) {
                _setSubPropertyByString(obj, keys[i], values[i]);
            }

            return obj;
        };

    var _unflattenFromArray = function (arrKeyValue) {
            var obj = {};
            var arrKeyValueLength = arrKeyValue.length;

            for (var i = 0; i < arrKeyValueLength; i++) {
                _setSubPropertyByString(obj, arrKeyValue[i][0], arrKeyValue[i][1]);
            }

            return obj;
        };



    // exports.flattenKeyValue = function (data){
    //   return _flattenKeyValue(data);
    // };
    // exports.flattenKey = function (data){
    //   return _flattenKey(data);
    // };
    // exports.flattenValue = function (data){
    //   return _flattenValue(data);
    // };
    return {
        unflattenFromCouples: _unflattenFromCouples,
        unflattenFromArray: _unflattenFromArray,
        getSubPropertyByString: _getSubPropertyByString,
        setSubPropertyByString: _setSubPropertyByString,
        flattenKeyValue: function (data) {
            return _flattenKeyValue(data);
        },
        flattenKey: function (data) {
            return _flattenKey(data);
        },
        flattenValue: function (data) {
            return _flattenValue(data);
        }
    };
};