var should   = require('should');
var assert   = require('assert');

var ObjectId = require('mongoose').Schema.Types.ObjectId;
var utils    = require('../lib/ojsutils')([ObjectId]);

var debug    = require('debug')('test:ojsutils');

describe('UTILS ', function(done){

  var date = new Date();
  var oid  = new ObjectId();

  var oracleObj = {
    profile: {
      firstName  : 'firstname value',
      secondName : 'secondname value',
      country : {
        name : 'Italy',
        code : 'IT'
      },
      profileImageUrl : 'profile image url value'
    },
    id: oid,
    email: 'alessandro.maccagnan@gmail.com',
    pippo: 'pippo value',
    country : 'just to be tricky',
    modifiedAt : date
  };

  var oracleObjKeys = [
    'profile.firstName',
    'profile.secondName',
    'profile.country.name',
    'profile.country.code',
    'profile.profileImageUrl',
    'id',
    'email',
    'pippo',
    'country',
    'modifiedAt'
  ];

  var oracleObjValues = [
    'firstname value',
    'secondname value',
    'Italy',
    'IT',
    'profile image url value',
    oid,
    'alessandro.maccagnan@gmail.com',
    'pippo value',
    'just to be tricky',
    date
  ];

  var oracleObjKeyValue = [
    ['profile.firstName','firstname value'],
    ['profile.secondName','secondname value'],
    ['profile.country.name','Italy'],
    ['profile.country.code','IT'],
    ['profile.profileImageUrl','profile image url value'],
    ['id',oid],
    ['email','alessandro.maccagnan@gmail.com'],
    ['pippo','pippo value'],
    ['country','just to be tricky'],
    ['modifiedAt',date],
  ];

  var newoid = new ObjectId();
  var newdate = new Date();
  var newObjValues = [
    'new firstname value',
    'new secondname value',
    'Spain',
    'SP',
    'newprofile image url value',
    newoid,
    'alessandro.maccagnan+new@gmail.com',
    'new pippo value',
    'new just to be tricky',
    newdate
  ];



  it('should flatten the properties of an object into an array', function(done) {
    objFlat = utils.flattenKeyValue(oracleObj);
    assert.deepEqual(objFlat, oracleObjKeyValue, 'something wrong');
    done();
  });

  it('should build an object from an array of keys and one of values', function(done){
    var unflattenObj = utils.unflattenFromCouples(oracleObjKeys, oracleObjValues);
    assert.deepEqual(unflattenObj, oracleObj, 'oh oh');
    done();
  });

  it('should build an object from an array of [keys/values]', function(done){
    var unflattenObj            = utils.unflattenFromArray(oracleObjKeyValue);
    assert.deepEqual(unflattenObj, oracleObj, 'oh oh');
    done();
  });

  it('should modify only "leaves"', function(done){
    var mongoose       = require('mongoose');
    var Schema         = mongoose.Schema;
    var userSchema = new Schema({

      email                  : { type: String, required : true, unique:true },
      password               : { type: String, required : true },

      profile           :  {
        firstName         : { type: String, 'default': null },
        img               : {
          me: {
           small    : { type: String, 'default': null },
           thumb    : { type: String, 'default': null }
          }
        },
      },//end-profile
      languages :[{
        code: { type: String },
        type: { type: String },
      }]

    }, { strict: true });

    var UserModel = mongoose.model('User', userSchema);



    var user = new UserModel();
    utils.setSubPropertyByString(user, 'profile.firstName', 'pippo');
    user.isDirectModified('profile').should.be.false;
    done();
  });

  it('should Get sub values', function(done) {
    debug('');

    var subkeys   = null;
    var subvalues = null;
    for (var i = 0; i < oracleObjKeys.length; i++) {
      subkey = oracleObjKeys[i];
      subvalue = utils.getSubPropertyByString(oracleObj, subkey);
      debug('oracleObj[%j] = %j', subkey, subvalue);
      subvalue.should.be.equal(oracleObjValues[i]);
    };

    done();
  });

  it('should Set sub values', function(done) {
    debug('');

    var subkeys   = null;
    var objFlat = utils.flattenKeyValue(oracleObj);

    for (var i = 0; i < objFlat.length; i++) {
      subkey = objFlat[i][0];
    
      utils.setSubPropertyByString(oracleObj, subkey, newObjValues[i]);
    

      debug('oracleObj[%j] = %j', subkey, oracleObj[subkey]);
    };

    var k, v;
    var newObjFlat = utils.flattenKeyValue(oracleObj);
    for (var i = 0; i < newObjFlat.length; i++) {
      k = newObjFlat[i][0];
      v = newObjFlat[i][1];
      newV =newObjValues[i];
      debug('newObjFlat[%j] = %j', k, v);
      v.should.be.equal(newV);
    }

    done();
  });



});







